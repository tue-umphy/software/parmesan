🧀 PARMESAN - Meteorological Timeseries and Turbulence Analysis in Python 💨🌦️
==============================================================================

PARMESAN is a Python package for meteorological timeseries and turbulence analysis.
One could interpret its acronym as the **P**\ ython **A**\ tmospheric **R**\ esearch Package for **Me**\ teorological Time\ **S**\ eries **An**\ alysis.

Many of PARMESAN's functions are backed by :mod:`sympy` expressions.
This means that the underlying equations can be rearranged (see :any:`get_function`) and recombined easily and also queried for the dependent variables.
A consequence of this is that most of PARMESAN's API documentation is automatically generated, even with pretty LaTeX representations of the equations.
PARMESAN makes sure its equations have correctly balanced physical units (it employs :mod:`pint`) and can convert function inputs and outputs automatically to the correct unit.
It will also find abnormal values in your data and warn you in that case (e.g. negative mass densites).

✨ Features
+++++++++++

What can you do with :mod:`parmesan`?

- ✅ be sure you didn't mess up physical units (:any:`units.ensure`) or value ranges (:any:`bounds.ensure`)
- 🔢 calculate and convert between typical meteorological parameters
    - 🌡️ virtual/potential temperature (:any:`parmesan.gas.temperature`)
    - 💦 mixing ratio, specific humidity, absolute humidity, water vapour pressure, ...  (:any:`parmesan.gas.humidity`)
    - 💨 convert between several wind speed/direction representations (:any:`parmesan.wind`)
    - ⚖️  density (:any:`parmesan.gas.density`) and gas concentrations (:any:`parmesan.gas.conversion`)
    - 🏔️ pressure extrapolation (:any:`parmesan.gas.pressure`)
- 💨 Turbulence Analysis
    - 🔢 calculate turbulence parameters (:any:`parmesan.turbulence`)
    - 💨 flux-gradient eddy fluxes (:any:`parmesan.flux`)
    - 🔃 calculate running covariances (:any:`covariances`)
    - 📉 Units-aware proper variance spectra with Parseval's Theorem asserted (:any:`parmesan.analysis.spectrum`)
    - 📈 structure function (:any:`parmesan.analysis.structure`)
    - 📉 autocorrelation function (:any:`parmesan.analysis.autocorrelation`)
- 🪨 Atmospheric Stability parameters (:any:`parmesan.stability`)
    - 📏 Monin-Obukov length (:any:`parmesan.stability.monin_obukov_length`)
    - 🧵 Monin-Obukov stability parameter and function (e.g. :any:`parmesan.stability.monin_obukov_stability_function_momentum_dyer_hicks`)
- 🎓 Accesss the underlying :mod:`sympy` expressions of functions (:mod:`parmesan.symbols`)
    - 👆 Access the sensitivity/error analysis equations and functions as well! (:any:`maximum_error_equation`)
    - 🧩 Analytically compose new custom equations and functions from the given functions and turn them into equally powerful Python functions (:any:`from_sympy` decorator)
    - 🤖 Let PARMESAN `find (and even rearrange) <notebooks/building-on-parmesan.html#Finding-Functions-for-your-desired-inputs-and-outputs>`_ equations for the quantities you need.
- 🌄 calculate diurnal/seasonal/etc. cycles (:any:`temporal_cycle`)
- ...

Check out the :doc:`examples` for detailed usage instructions. For a more detailed demo of the internals, see :doc:`notebooks/building-on-parmesan`.

❓ Why not :mod:`metpy`?
++++++++++++++++++++++++

:mod:`metpy` (with its :mod:`metpy.calc` module) already provides an awesome lot of functionality to handle and visualize weather data. 
However, it seems its focus is rather on spatially distributed data and **less on timeseries and turbulence analysis**.
Both :mod:`parmesan` and :mod:`metpy` employ :mod:`pint` to handle physical units but lacks :mod:`parmesan`'s configurable **convenience and safety net for automatic input/output unit conversion and bounds assertion** (see :doc:`settings`).
In addition, many of :mod:`parmesan`'s functions are backed by :mod:`sympy` expressions, providing the full flexibility to compose new equations from them (e.g. by substitution, differentiation or integration) and even to do **sensitivity/error analysis** (:any:`maximum_error_equation`).


🚀 Performance
++++++++++++++

PARMESAN is **not** built for speed! It focuses on usage convenience and to minimize common errors made when doing physical calculations - the units and the math! In fact, as most of PARMESAN's functions are auto-generated (and validated) based on SymPy expressions, all functions are actually rebuilt and re-checked for units validity whenever you import PARMESAN. Together with its dependencies, this might cause it to take a couple of seconds to import.

If you find that PARMESAN is a bottleneck in your application, you can try these tweaks:

.. code-block:: sh

   # set this environment variable to skip error analysis generation 
   export PARMESAN_SKIP_ERROR_ANALYSIS=yes
   # set this environment variable to skip equation units checking upon import
   export PARMESAN_SKIP_UNITS_CHECK=yes
   # set this environment variable to skip importing all equations,
   # which can add up to a significant delay when importing PARMESAN
   # Keep in mind that you will have to import the respective submodules manually
   # and that equation lookup functions like get_function() will only see equations
   # from modules that you actually imported.
   export PARMESAN_SKIP_EQUATION_IMPORT=yes

.. code-block:: python

    import parmesan
    parmesan.units.mode.state = None    # Not recommended: disable function call units check
    parmesan.bounds.mode.state = None   # Not recommended: disable function call bounds check


👥 Who's behind it?
+++++++++++++++++++

:mod:`parmesan` is mainly developed by Yann Büchau at the `Environmental Physics Workgroup (Umphy) <https://uni-tuebingen.de/de/84450>`_ at the University of Tübingen.

You can find the source code `on GitLab <https://gitlab.com/tue-umphy/software/parmesan>`_.

..
   We hide this warning for now as the new SymPy way prevents the coverage test
   from actually determining if a function has been called in the tests.

    .. warning::

        Before relying on the output of functions in :mod:`parmesan`, make sure
        they are tested, for example by checking the `Coverage Report
        <coverage-report/index.html>`_.

        The `Coverage Report <coverage-report/index.html>`_ is generated
        automatically from the `test suite
        <https://gitlab.com/tue-umphy/software/parmesan/-/tree/master/tests>`_ in the
        `Pipelines <https://gitlab.com/tue-umphy/software/parmesan/-/pipelines>`_.

        Browse the report for the function you are using and make sure it's marked
        **green** (tested) and not **red** (untested).

        If it is red, consider `writing a test
        <https://gitlab.com/tue-umphy/software/parmesan/-/blob/master/CONTRIBUTING.md#writing-tests>`_
        to make sure it is working as intended.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   settings
   examples
   api/modules
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
