{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "84b1b206-a5c4-471e-ab87-9953e1f92d06",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 🎓 Parseval's Theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9aa2de38-f92d-4aa2-a15d-04e2f0da09d5",
   "metadata": {},
   "source": [
    "In this notebook, [Parseval's Theorem](https://en.wikipedia.org/w/index.php?title=Parseval%27s_theorem&oldid=1062705638#Notation_used_in_engineering) according to [Stull (1988)](http://dx.doi.org/10.1007/978-94-009-3027-8) is verified to be valid with PARMESAN's spectra."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09dbbf32-d273-4718-a03d-69537bc2a70e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.dates\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "np.random.seed(420)\n",
    "import pandas as pd\n",
    "import scipy.fft\n",
    "import scipy.optimize\n",
    "import scipy.signal\n",
    "\n",
    "import parmesan"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3b457023-bdbe-426e-9f11-2495b5171be3",
   "metadata": {
    "tags": []
   },
   "source": [
    "## ⚙ Generating a Signal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95be7434-2449-445a-84b9-e5c5745a7824",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10000\n",
    "sampling_rate = 9  # Hz\n",
    "spectrum_kwargs = dict(window=None, detrend=\"linear\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca3403e1-47bc-457f-817c-200c1f483a13",
   "metadata": {},
   "outputs": [],
   "source": [
    "# times to use\n",
    "seconds = np.arange(0, n, 1) / sampling_rate\n",
    "timestamps = pd.to_datetime(seconds, unit=\"s\", origin=\"now\")\n",
    "\n",
    "# generate a random walk signal as base\n",
    "random_walk = np.cumsum((np.random.random(n) - 0.5) * 2)\n",
    "\n",
    "# generate a wave with a much lower frequency than the sampling rate\n",
    "wave_frequency = sampling_rate / 50\n",
    "wave_amplitude = random_walk.var() / 100\n",
    "wave = wave_amplitude * np.sin(2 * np.pi * wave_frequency * seconds)\n",
    "\n",
    "df = pd.DataFrame(\n",
    "    dict(signal=random_walk + wave),\n",
    "    index=timestamps,\n",
    ")\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdf7addb-1e04-4b6d-a4a0-7db5abbcad3d",
   "metadata": {},
   "source": [
    "## 🔧 Function to Plot Timeseries and Spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9dc1743e-1769-4666-94ed-e29acdf69e5b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def plot_spectrum(**spectrum_kwargs):\n",
    "    from numpy import abs, mean, sum\n",
    "\n",
    "    plt.rcParams[\"figure.figsize\"] = (10, 12)\n",
    "    plt.rcParams[\"font.size\"] = 13\n",
    "    plt.rcParams[\"legend.labelspacing\"] = 1.5\n",
    "    plt.rcParams[\"legend.loc\"] = \"lower left\"\n",
    "    fig, (ax1, ax2) = plt.subplots(nrows=2)\n",
    "\n",
    "    spectrum_kwargs_str = \", \".join(\n",
    "        \"{}={}\".format(k, repr(v)) for k, v in spectrum_kwargs.items()\n",
    "    )\n",
    "\n",
    "    df.signal.plot(\n",
    "        ax=ax1,\n",
    "        label=f\"random walk + ${wave_amplitude:.2f} \\cdot \\sin({wave_frequency:.2f}\\mathrm{{Hz}})$\\n\"\n",
    "        f\"mean = {df.signal.mean():.2f}, variance $\\sigma^2$ = {df.signal.var():.2f}\",\n",
    "    )\n",
    "\n",
    "    (\n",
    "        freq,\n",
    "        power,\n",
    "        blocks,\n",
    "    ) = parmesan.analysis.variance_spectrum(\n",
    "        x=seconds,\n",
    "        y=df.signal.values,\n",
    "        # we also want variance_spectrum() to return the actual data it used for the FFT, so we instruct it to also return the \"blocks\"\n",
    "        returnvalue=(\"frequency\", \"power\", \"blocks\"),\n",
    "        **spectrum_kwargs,\n",
    "    )\n",
    "    # put the conditioned data into the dataframe\n",
    "    df[\"conditioned\"] = blocks[0].m  # we only have one block\n",
    "\n",
    "    df.conditioned.plot(\n",
    "        ax=ax1,\n",
    "        label=f\"conditioned signal ({spectrum_kwargs_str}) used in FFT\\n\"\n",
    "        f\"mean = {df.conditioned.mean():.2f}\\n\"\n",
    "        f\"$\\\\mathbf{{variance}}$ $\\sigma^2 = \\\\mathbf{{{df.conditioned.var(ddof=0) = :.4f}}}$\\n\"\n",
    "        f\"$= \\\\mathtt{{{mean((df.conditioned-df.conditioned.mean())**2) = :.4f}}}$\\n\",\n",
    "    )\n",
    "\n",
    "    ax1.set_title(r\"Timeseries\")\n",
    "    ax1.legend()\n",
    "\n",
    "    samplingrate = sampling_rate\n",
    "    N = df.signal.size\n",
    "    plot = (ax2.plot if spectrum_kwargs.get(\"density\", False) else ax2.stem)(\n",
    "        freq,\n",
    "        power,\n",
    "        label=f\"$\\\\mathtt{{variance\\\\_spectrum(}}${spectrum_kwargs_str}${{)}}$ of signal,\\n\"\n",
    "        + (\n",
    "            (\n",
    "                f\"$\\\\mathbf{{variance}} = 2 \\cdot \\int_{{f>0}}\\\\left|X_{{FFT, i}}\\\\right|^2 df  \"\n",
    "                + f\"= \\\\mathtt{{\\\\mathbf{{\"\n",
    "                + (\n",
    "                    f\"{power[1:].m.sum()*samplingrate/N  = :.4f}\"\n",
    "                    if spectrum_kwargs.get(\"double\", True)\n",
    "                    else f\"{2*power[1:].m.sum()*samplingrate/N = :.4f}\"\n",
    "                )\n",
    "                + f\"}}}}$\\n\"\n",
    "            )\n",
    "            if spectrum_kwargs.get(\"density\", False)\n",
    "            else (\n",
    "                f\"$\\\\mathbf{{variance}} = 2 \\cdot \\sum_{{i=1}}^{{{len(df.index)}}}\\\\left|X_{{FFT, i}}\\\\right|^2  \"\n",
    "                + f\"= \\\\mathtt{{\\\\mathbf{{\"\n",
    "                + (\n",
    "                    f\"{power[1:].sum() = :.4f}\"\n",
    "                    if spectrum_kwargs.get(\"double\", True)\n",
    "                    else f\"{2*power[1:].sum() = :.4f}\"\n",
    "                )\n",
    "                + f\"}}}}$\\n\"\n",
    "            )\n",
    "        ),\n",
    "        **(\n",
    "            dict(markerfmt=\"none\", basefmt=\"none\")\n",
    "            if not spectrum_kwargs.get(\"density\", False)\n",
    "            else dict(\n",
    "                color=ax1.get_lines()[-1].get_color(), drawstyle=\"steps-mid\"\n",
    "            )\n",
    "        ),\n",
    "    )\n",
    "    if not spectrum_kwargs.get(\"density\", False):\n",
    "        plot[1].set_color(ax1.get_lines()[-1].get_color())\n",
    "\n",
    "    spectrum = df.signal.parmesan.spectrum(\n",
    "        with_kolmogorov=True, **spectrum_kwargs\n",
    "    )\n",
    "\n",
    "    # spectrum.filter(axis=\"columns\", regex=\"^signal$\").squeeze().plot(\n",
    "    #     ax=ax2,\n",
    "    #     label=f\"$\\\\mathtt{{df.signal.parmesan.spectrum({spectrum_kwargs_str})}}$\\nyields the same variance spectrum\",\n",
    "    #     linestyle=\"dotted\",\n",
    "    #     alpha=0.5,\n",
    "    #     color=\"green\",\n",
    "    # )\n",
    "    spectrum.filter(axis=\"columns\", regex=\"power.*law\").squeeze().pint.m.plot(\n",
    "        ax=ax2, color=\"black\", linestyle=\"dashed\"\n",
    "    )\n",
    "\n",
    "    ax2.axvline(\n",
    "        sampling_rate,\n",
    "        label=f\"{sampling_rate = } Hz\",\n",
    "        linestyle=\"dotted\",\n",
    "        linewidth=5,\n",
    "        color=\"black\",\n",
    "        alpha=0.1,\n",
    "        zorder=-1,\n",
    "    )\n",
    "    ax2.axvline(\n",
    "        wave_frequency,\n",
    "        label=f\"{wave_frequency = } Hz\",\n",
    "        linestyle=\"solid\",\n",
    "        linewidth=10,\n",
    "        color=\"black\",\n",
    "        alpha=0.1,\n",
    "        zorder=-1,\n",
    "    )\n",
    "    ax2.set_title(\n",
    "        (\n",
    "            \"Spectral Energy Density\"\n",
    "            if spectrum_kwargs.get(\"density\", False)\n",
    "            else \"Discrete Variance Spectrum\"\n",
    "        )\n",
    "        + (\n",
    "            f\" acc. to Stull\"\n",
    "            if spectrum_kwargs.get(\"norm\", True)\n",
    "            and spectrum_kwargs.get(\"double\", True)\n",
    "            else \"\"\n",
    "        )\n",
    "    )\n",
    "    ax2.set_xscale(\"log\")\n",
    "    ax2.set_yscale(\"log\")\n",
    "    ax2.legend()\n",
    "    ax1.set_ylabel(\"signal\")\n",
    "    ax2.set_ylabel(\n",
    "        \"signal² / Hz\" if spectrum_kwargs.get(\"density\", False) else \"signal²\"\n",
    "    )\n",
    "\n",
    "    fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29248abd-d5ea-4c4f-ad90-95aab93868c1",
   "metadata": {},
   "source": [
    "## ✅ Verifying Parseval's Theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25300320-cf6d-47ff-b46b-2155d172f299",
   "metadata": {},
   "source": [
    "Note how in all the below plots the *variance* of the signal equals the *energy* in the frequency domain (the **bold** numbers in the legends below 👇)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a4b69e1-c6af-4982-bbe2-a6dede996f63",
   "metadata": {},
   "source": [
    "According to Stull (1988) chapter 8 eq. 8.5a and 8.6.1b for a signal $A$ with $N$ sampling points:\n",
    "\n",
    "$$\n",
    "\\mathrm{variance}\n",
    "=\n",
    "\\sigma_\\mathrm{A} ^ 2\n",
    "=\n",
    "\\frac{1}{N} \\sum_{k=0}^{N-1} \\left( A_\\mathrm{k} - \\overline{A} \\right) ^ 2\n",
    "=\n",
    "\\sum_{k=1}^{N-1} \\left| F_\\mathrm{A}\\left(k\\right) \\right| ^ 2\n",
    "$$\n",
    "\n",
    "Where $F_\\mathrm{A}\\left(n\\right)$ is calculated from the signal $A$ divided by the amount of sampling points $N$.\n",
    "\n",
    "<details>\n",
    "<summary>On Wikipedia</summary>\n",
    "    \n",
    "The [Wikipedia Article detailing Parseval's Theorem](https://en.wikipedia.org/w/index.php?title=Parseval%27s_theorem&oldid=1062705638#Notation_used_in_engineering) puts it like this:\n",
    "\n",
    "$$\n",
    "\\sum_{n=0}^{N-1} | x[n] |^2  = \\frac{1}{N} \\sum_{k=0}^{N-1} | X[k] |^2\n",
    "$$\n",
    "\n",
    "The left-hand side $\\sum_{n=0}^{N-1} | x[n] |^2$ is the signal energy and divided by the sample size $N$ becomes the sample variance in case the signal averages to 0, i.e. the signal is just annomalies.\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "891261ef-00c3-4d14-812d-51654217d5e6",
   "metadata": {},
   "source": [
    "### Linear Detrending, then Hann Window"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37628e25-e676-42ea-9ed2-48a4c22549ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_spectrum(detrend=\"linear\", window=\"hann\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abc3cef8-c64f-45b6-8976-143487698a16",
   "metadata": {},
   "source": [
    "(We display the discrete variance spectrum as a stem plot to emphasize that it's individual, discrete values, not a continuous graph.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65db7089-82a8-411b-8b82-cfc0e926cd1e",
   "metadata": {},
   "source": [
    "### Linear Detrending, then Tukey Window"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1081d50d-6214-466f-89e5-39a67faea4f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_spectrum(window=\"tukey\", detrend=\"linear\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7dea5276-464c-4daf-8927-56ed10e9fea9",
   "metadata": {},
   "source": [
    "### Linear Detrending, no Window"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34cc9502-4413-4c5b-b147-960054293a03",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_spectrum(window=None, detrend=\"linear\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90500f89-0c01-4948-b3a7-c4c544316cba",
   "metadata": {},
   "source": [
    "### No Detrending, no Window"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5694ed66-e486-4e2d-8cfc-68fc5d851e2a",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plot_spectrum(window=None, detrend=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8a50a2d-a185-4d85-b20c-1ffff7e3e91b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plot_spectrum(double=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a353bbc-f19f-469c-ab8d-2569e55d36ab",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plot_spectrum(density=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fdca8a29-e19e-4ed0-8372-5400ee174718",
   "metadata": {},
   "source": [
    "(Here we display the spectral energy density as a continuous graph as we are integrating it.)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
