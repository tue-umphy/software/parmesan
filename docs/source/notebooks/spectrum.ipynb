{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 📉 Calculating a Spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "\n",
    "import pandas as pd\n",
    "\n",
    "import parmesan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create some data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You would normally read this from a CSV file for example. Let's generate oscillating signals containing some weaker and stronger frequencies, a linear trend and some random walk noise:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(42)\n",
    "t = np.arange(0, 50, 0.01)\n",
    "mainfreqs = [3, 2, 0.5, 0.1]\n",
    "mainfreq = iter(mainfreqs)\n",
    "data = pd.DataFrame(\n",
    "    {\n",
    "        \"wind\":\n",
    "        # smaller sine\n",
    "        1.5 * np.sin(t * 2 * np.pi * next(mainfreq))\n",
    "        # larger cosine\n",
    "        + 3 * np.cos(t * 2 * np.pi * next(mainfreq))\n",
    "        # linear trend\n",
    "        + 0.5 * t - 4\n",
    "        # random walk noise\n",
    "        + np.cumsum((np.random.random(t.size) - 0.5) / 0.5),\n",
    "        \"temperature\":\n",
    "        # smaller sine\n",
    "        1.5 * np.sin(t * 2 * np.pi * next(mainfreq))\n",
    "        # larger  cosine\n",
    "        + 3 * np.cos(t * 2 * np.pi * next(mainfreq))\n",
    "        # linear trend\n",
    "        - 0.1 * t + 10\n",
    "        # random walk noise\n",
    "        + 0.1 * np.cumsum((np.random.random(t.size) - 0.5) / 0.5),\n",
    "    },\n",
    "    index=pd.to_datetime(t, unit=\"s\", origin=datetime.datetime.now()),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also add a unit to our measurement (optional):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[\"wind\"] = data[\"wind\"].astype(\"pint[m/s]\")\n",
    "data[\"temperature\"] = data[\"temperature\"].astype(\"pint[°C]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is our data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.pint.dequantify()  # the .pint.dequantify()  shows the units as extra columns level"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is what it looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.pint.dequantify().plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 🔢 Calculating the spectrum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are severeal ways to use `parmesan`'s spectrum calculation function. The simplest is directly from the `pandas` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.parmesan.spectrum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The units are also calculated correctly:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note how non-multiplicative units (**°C** here) are automatically converted to make the calculations work.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.parmesan.spectrum().pint.dequantify()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even if we calculate the spectral variance density:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.parmesan.spectrum(density=True).pint.dequantify()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 📉 Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also directly plot a spectrum via `pandas`' plotting capabilities:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[\"wind\"].parmesan.spectrum(density=True).plot(legend=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "We plot the spectral variance *density* with a **continuous line** here. **Discrete** spectral variances (`density=False`, the default) shouldn't be visualized with a continuous line but rather with e.g. a [stem plot](https://matplotlib.org/stable/gallery/lines_bars_and_markers/stem_plot.html#sphx-glr-gallery-lines-bars-and-markers-stem-plot-py).\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also plot both spectra, but [`pint`'s `matplotlib` support](https://pint.readthedocs.io/en/stable/user/plotting.html) doesn't (yet) like multiple units per axis, so we need to `pint.dequantify()` the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.parmesan.spectrum(density=True).pint.dequantify().plot(legend=True);"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Discrete variance spectrum with proper stem plot and if you'd like to see it on a double-logarithmic scale also with a Kolmogorov power-law fit from the very beginning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for col, d in data[\"wind\"].parmesan.spectrum(with_kolmogorov=True).items():\n",
    "    if \"power-law\" in col:\n",
    "        d.plot(color=\"black\", label=col)\n",
    "    else:\n",
    "        plt.stem(d.index, d, markerfmt=\"none\", label=col)\n",
    "plt.legend()\n",
    "plt.loglog();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Hint**: In an interactive `matplotlib` plot, you can move the mouse into the plotting area and press the `K` or `L` key to toggle logarithmic axes scale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "data.parmesan.spectrum(density=True).pint.dequantify().iloc[1:].plot(\n",
    "    ax=ax\n",
    ")  # ignore zero-frequency for log-plot\n",
    "plt.loglog()\n",
    "# mark the dominant frequencies\n",
    "style = plt.rcParams[\"axes.prop_cycle\"]()\n",
    "for freq in mainfreqs:\n",
    "    ax.axvline(\n",
    "        freq,\n",
    "        label=f\"{freq} Hz\",\n",
    "        zorder=-1,\n",
    "        linestyle=\"dashed\",\n",
    "        alpha=0.2,\n",
    "        linewidth=5,\n",
    "        **(next(style)),\n",
    "    )\n",
    "ax.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can read the follwing information from this spectrum:\n",
    "\n",
    "- In general, in this case, *temperature* has **lower variance** over all pretty much frequencies. This is also visible in the time series plot above.\n",
    "- *wind* has **faster dominant fluctuations** (the two dominant frequencies are higher)\n",
    "- PARMESAN's `[variance_]spectrum` function correctly identifies the dominant frequencies in the signals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 🎓 Parseval's Theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the default settings, `spectrum()` and `variance_spectrum()` produce a **discrete variance spectrum**, i.e. [Parseval's Theorem](https://en.wikipedia.org/wiki/Parseval%27s_theorem) is valid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    var := data[\"wind\"].var(ddof=0)\n",
    ")  # ddof=0 to divide by N, definition of Parseval's Theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "👆 This is the variance of the original signal. According to Parseval's Theorem, this should equal the sum of spectral variances in the discrete variance spectrum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(sum := data[\"wind\"].parmesan.spectrum().sum())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if np.allclose(var, sum):\n",
    "    print(\n",
    "        f\"✅ Signal variance equals total discrete spectral variance, Parseval's Theorem is valid with PARMESAN!\"\n",
    "    )\n",
    "else:\n",
    "    print(f\"💥 Oh no, this shouldn't happen! 😲\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a more in-depth validation of Parseval's Theorem in PARMESAN, look [here](spectrum-parseval-theorem.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ⏱️ Non-Timeseries Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `variance_spectrum` function can handle more than just timeseries data. If you have data from an intensity data from an interferometer for example in relation to distance and need to find the dominant wavenumber, you could do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from parmesan.units import units\n",
    "from pint_pandas import PintArray\n",
    "\n",
    "distance = units.Quantity([1, 2, 3, 4, 5], \"mm\")\n",
    "intensity = units.Quantity([3, 2, 1, 3, 5], \"W/m²\")\n",
    "wavenumber, variance = parmesan.analysis.variance_spectrum(distance, intensity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavenumber"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "variance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.DataFrame(\n",
    "    {\"spectral variance\": PintArray(variance)},\n",
    "    index=pd.Index(PintArray(wavenumber), name=\"wavenumber\"),\n",
    ").pint.dequantify()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
