{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 🔄 Temporal Cycles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "import parmesan\n",
    "from parmesan.units import units"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download and parse some history temperature data for Stuttgart from the [German Weather Service's Open Data Collection](https://opendata.dwd.de/climate_environment/CDC/):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = pd.read_csv(\n",
    "    \"https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/10_minutes/air_temperature/historical/10minutenwerte_TU_04926_20020621_20091231_hist.zip\",\n",
    "    delimiter=\";\",\n",
    "    # nrows=50000,  # uncomment this to use less data\n",
    "    parse_dates=[\"MESS_DATUM\"],\n",
    "    index_col=\"MESS_DATUM\",\n",
    "    na_values=[-999],\n",
    "    usecols=[\"MESS_DATUM\", \"TT_10\"],\n",
    "    infer_datetime_format=True,\n",
    ")\n",
    "data.tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 📈 Plot the Timeseries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, (axleft, axright) = plt.subplots(ncols=2, figsize=(15, 6))\n",
    "data[\"TT_10\"].plot(ax=axleft, legend=True)\n",
    "axleft.set_title(\"Temperature in Stuttgart over the Years\")\n",
    "data[\"TT_10\"].loc[\"2002-08-01\":\"2002-09-01\"].plot(ax=axright, legend=True)\n",
    "axright.set_title(\"Temperature in Stuttgart over one Month\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "☝ There are clearly two dominant cycles here: a **seasonal** (yearly) cycle and a **diurnal** (daily) cycle."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 📉 Plot a Spectrum "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There dominant cycles can be also seen in a spectrum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()  # new plot\n",
    "#                                   👇 optional unit 👇\n",
    "data[\"TT_10\"].interpolate().dropna().astype(\n",
    "    \"pint[°C]\"\n",
    ").parmesan.spectrum().plot(\n",
    "    ax=ax\n",
    ")  # plot the spectrum\n",
    "props = iter(\n",
    "    tuple(plt.rcParams[\"axes.prop_cycle\"])\n",
    ")  # a changing iterator of matplotlib colors\n",
    "# show typical periods as vertical lines\n",
    "for interval in (\n",
    "    1 * units(\"year\"),\n",
    "    1 * units(\"day\"),\n",
    "    12 * units(\"hours\"),\n",
    "    # in this example, the harmonics of 1 day are also clearly visible\n",
    "    24 / 3 * units(\"hours\"),\n",
    "    24 / 4 * units(\"hours\"),\n",
    "    24 / 5 * units(\"hours\"),\n",
    "    24 / 6 * units(\"hours\"),\n",
    "    # ...\n",
    "):\n",
    "    ax.axvline(\n",
    "        (1 / interval).to(\"Hz\").m,\n",
    "        linestyle=\"dashed\",\n",
    "        label=interval,\n",
    "        zorder=-1,\n",
    "        **next(props),\n",
    "    )\n",
    "ax.set_xscale(\"log\")\n",
    "ax.set_yscale(\"log\")\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the **seasonal** (yearly) cycle is the most prominent one (highest peak), followed by the **diurnal** (daily) cycle. \n",
    "\n",
    "The harmonics of the 1-day period (integer divisions of 24 hours) are also visible, but don't matter much here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 🌄🌇 Diurnal Cycle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `temporal_cycle()`-[function](https://tue-umphy.gitlab.io/software/parmesan/api/parmesan.aggregate.html#parmesan.aggregate.temporal_cycle) we can aggregate the whole timeseries into time blocks and operate on them, e.g. build an average for every hour of the day:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# aggregate every hour of a day\n",
    "diurnal_cycle = data.parmesan.temporal_cycle(\n",
    "    interval=\"D\",  # We want to aggregate data for every Day (could also use \"Y\" for year)\n",
    "    resolution=\"h\",  # let's aggregate to every hour (could also use \"m\" for minute)\n",
    ")\n",
    "# Build the average over each hour of a day\n",
    "mean_diurnal_cycle = diurnal_cycle.mean()\n",
    "# Plot it\n",
    "fig, ax = plt.subplots()\n",
    "mean_diurnal_cycle[\"TT_10\"].plot(\n",
    "    ax=ax, legend=True, label=\"mean diurnal cycle\"\n",
    ")\n",
    "ax.set_xlabel(\"hour of day\")\n",
    "ax.set_ylabel(\"Temperature [°C]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 🌱🌞🍃🌨 Seasonal Cycle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also plot the **seasonal** (yearly) cycle quickly in one line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[\"TT_10\"].parmesan.temporal_cycle(\n",
    "    interval=\"Y\", resolution=\"D\"\n",
    ").mean().plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "☝ The x-Axis is the day in the year, the y-Axis the average temperature at that day across all years in the dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 🔎 Finer Resolution Control"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With only `interval` and `resolution`, we are limited to `numpy.datetime`'s [units](https://numpy.org/doc/stable/reference/arrays.datetime.html#datetime-units) intervals (e.g. `D` for day, `h` for hour, etc...).\n",
    "\n",
    "We can fine-tune that with the `modifier` argument. Suppose we want a seasonal cycle, but with a resolution of **10 days**, not one (e.g. to smooth out the curve):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[\"TT_10\"].parmesan.temporal_cycle(\n",
    "    interval=\"Y\",  # we want a seasonal cycle (blocks of years)\n",
    "    resolution=\"D\",  # we want daily resolution\n",
    "    # but we reduce that resolution by a factor of 10\n",
    "    modifier=lambda days: (days / 10).astype(int),\n",
    ").mean().plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "☝ The x-Axis is the number of the 10-day interval in the year, the y-Axis the average temperature at that day across all years in the dataset."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
