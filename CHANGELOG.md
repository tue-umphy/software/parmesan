# Changelog

## UNRELEASED

- `evenly_spaced_interval()`: warn, don't die in case of ambiguous results
- Add calculating humid air density from relative humidity (`density_humid_air_relative_humidity`)

## v2.1.1 (Thu Jul 25 2024)

- Fix absolute value of `spectrum(density=True)` (see [MR 51](https://gitlab.com/tue-umphy/software/parmesan/-/merge_requests/51), noticed by Julius Brenner, thank you!)

## v2.1.0 (Thu Jun 13 2024)

- Add Reynolds number calculation (`parmesan.turbulence.reynolds_number`)
- Improved LaTeX rendering (proper sub/superscript nesting, non-italic subscripts, non-italic multi-char variable names)
- restore old default `power_spectrum()` behaviour with `window="hann"` and `detrend="linear"`
- `potential_temperature()`: `p_ref` in Pa, not hPa
- performance:
    - allow skipping import of all equations by setting envvar `PARMESAN_SKIP_EQUATION_IMPORT=yes`. Equations submodules can be loaded later with `parmesan.load_equations()` or by specific manual import.
- documentation:
    - proper error on invalid positional argument usage in `@from_sympy`-decorated functions
    - add note on keyword-only arguments
    - better `parmesan.gas.constants` deprecation warning
    - suggest setting `PARMESAN_SKIP_EQUATION_IMPORT` if importing parmesan takes very long
- packaging:
    - poetry as packaging system
    - allow pandas v2

## v2.0.0 (Mo Aug 21 2023)

✨ SymPy Power! 🚀

In this big new point release most of PARMESAN's functions are rewritten
in terms of SymPy (<https://sympy.org>) equations. A sophisticated new
decorator `@from_sympy` then turns these equations into functions that
behave like typical PARMESAN functions do, but they can do even more and
have other benefits:

- Functions now have **unified argument names** in terms of
  `parmesan.symbols`. This means that every function depending on
  potential temperature will accept *exactly* the same argument name for
  potential temperature.
- Functions now accept any **alias** of symbols used in its expression
  (e.g. both `T` and `temperature`)
- Documentation (parameters, units, bounds, LaTeX formulas, etc. ) is
  now entirely auto-generated. This reduces the amount of boilerplate
  code for PARMESAN functions *immensely*. A full-featured PARMESAN
  function (units check, bounds check, auto-generated documentation,
  alias matching, etc.) can now be as short as *three lines* (decorator,
  function name and single-line SymPy expression). As you can imagine,
  this is a massive improvement for maintainability and code
  readability. The documentation will also contain less (human-style)
  errors such as copy-paste failures or missed parameter descriptions.
- Functions now have an *auto-generated* **maximum error sensitivity
  analysis** attached. This means if you want to know how uncertainties
  in your input values reflect in the result, you can use the
  `my_function.equation` attribute and it'll contain an executable
  function to calculate this. The documentation also explains the usage
  and displays the underlying equation.

Other Improvements:

Apart from those technical changes, PARMESAN now also features:

- several stability and flux functions.
- density functions
- a `covariances()` function to calculate running covariances, e.g. for
  eddy covariance analysis
- a `quicklook()` function to get a quick overview plot over a
  timeseries
- an `autocorrelation()` plot
- a proper `variance_spectrum()` function (deprecates
  <span class="title-ref">power_spectrum</span>\`) that fulfills
  Parseval's Theorem by default
- poetry as build system
- ... probably more 🙂

Backwards-Compatibility:

As this is a major point release, expect some breakages, although minor.
Some functions were renamed slightly and the exact parameter names have
been unified in terms of those in `parmesan.symbols`. Search the
documentation for the correct usage in case you encounter such errors.

## v1.1.0 (Di Jan 17 2023)

🐛 Several Bugfixes and more

- calculation of TKE
- calculation of LCL
- fix usage of Hann window
- add radiation functions
- fix bugs in virtual temperature and absolute humidity

## v1.0.2 (Fr Feb 11 2022)

📝 Fix README emojis on PyPI

## v1.0.1 (Fr Feb 11 2022)

📦 PyPI Upload

PARMESAN is now available on PyPI: <https://pypi.org/project/parmesan/>
🎉

## v1.0.0 (Fr Feb 11 2022)

🎉 Major Release

PARMESAN is stable enough for a major release number! 🎉

Changes since the last release include:

- calculation of the structure function

- example notebook for <span class="title-ref">temporal_cycle</span>
  added to the documentation

- improvements to \`spectrum\`:  
  - adherence to Parseval's Theorem asserted
  - fitting a Kolmogorov power-law to the spectrum
  - fine-tuneable windowing and detrending

- Yamartino wind averaging

- calculating head and cross wind components

- calculating the dewpoint from relative humidity

- added XKCD's geothmetic meandian: <https://xkcd.com/2435/>

## v0.3.0 (Mi Jan 20 2021)

Wind Direction and other Goodies

This release adds:

- wind direction calculation
- wind vector calculation
- generic vector angle calculations
- basic stats calculation (RMSE)
- more humidity measures

## v0.2.0 (Mi Dez 23 2020)

Bound Checking, Humidity and Temperature Calculations

This version complements the **unit checking mode** with a **bounds
checking mode** for sanity checks of input and output values of
functions.

The documentation was also improved to include pretty tables detailing
the units and bounds for inputs and outputs of physical calculation
functions.

Furthermore, several functions to calculate humidity measures were added
for:

- specific humidity
- absolute humidity
- mixing ratio
- water vapour pressure
- relative humidity

## v0.1.0 (Do Dez 17 2020)

Constants, Units, Spectra, Basic Gas Calculations

This release marks the first usable version of PARMESAN.

With PARMESAN you can now:

- calculate (and plot) power spectra with several settings
- convert between trace gas particle ratio and mass density
- calculate temporal (e.g. diurnal) cycles
- calculate potential temperature

All physical functions accept pandas objects, normal numbers and also
pint units. There is also an (optional) ”explicit unit mode” which makes
sure a pint unit is always handed with each value.

## v0.0.0 (Mi Dez 02 2020)

Initial Version

This version just provides an empty package with all the needed
infrastructure (documentation, tests, etc...)
